version = "1.0"

train {
    image = "asia.gcr.io/span-ai/ds_suite-pyspark:v1.0"
    script = ["python3 train.py"]

    parameters {
        ALPHA = "0.5"
        L5_RATIO = "0.5"
    }

    secrets = [
        "DUMMY_SECRET_A",
        "DUMMY_SECRET_B"
    ]
}

serve {
    image = "python:3.7"
    install = ["echo Something"]
    script = ["ls -la serve && python3 -v serve.py"]
}

features {
    image = "asia.gcr.io/span-ai/ds_suite-pyspark:v1.0"
    install = []
    script = ["python3 preprocess.py"]

    parameters {
        DUMMY_PARAMETER = "0.5"
        EMBEDDING_PATH = "gs://dummy-gcs-bucket/glove.6B.100d.txt"
    }

    secrets = [
        "DUMMY_SECRET_C",
        "DUMMY_SECRET_D"
    ]

    feature_definition {
        name = "pid"
        key = "product_id"
        description = "whether the product is cross-border"
    }

    feature_definition {
        name = "uid"
        key = "user_id"
        description = "whether the user is cross-border"
    }
}

batch_score {
    image = "asia.gcr.io/span-ai/ds_suite-pyspark:v1.0"

    script = ["python batch_score.py"]

    parameters {
        ALPHA = "0.1"
        L5_RATIO = "0.2"
    }

    secrets = [
        "DUMMY_SECRET_E",
        "DUMMY_SECRET_F"
    ]
}
