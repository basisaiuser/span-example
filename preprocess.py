import os
import pprint
import subprocess

from bedrock_client.bedrock.feature_store import get_feature_store

if __name__ == "__main__":
    subprocess.call(["cat", "bdrk.txt"])
    subprocess.call(["./colourtest.sh"])

    print("Environment variables")
    pprint.pprint(dict(os.environ))

    fs = get_feature_store()
    fs.write("pid", [("hahaha", 12341234)])
    fs.write("uid", [("nyancat", "meow"), ("doge", 56785678)])
