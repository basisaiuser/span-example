# https://gist.github.com/davidbgk/b10113c3779b8388e96e6d0c44e03a74
print("Initial")

import http.server
import os
import socketserver
from http import HTTPStatus

print("After import")


class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.end_headers()
        self.wfile.write(b"Hello world")


print("After class definition")


if __name__ == "__main__":
    port = int(os.environ.get("SERVER_PORT", "8080"))
    print(f"Starting server at {port}")
    httpd = socketserver.TCPServer(("", port), Handler)
    httpd.serve_forever()
