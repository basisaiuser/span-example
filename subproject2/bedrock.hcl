version = "1.0"

train {
    image = "basisai/spark-py:v2.4.0r7"
    install = []
    script = ["pwd && ls -la && python hello.py"]

    parameters {
        SUBPROJECT_TRAIN = "true"
        ALPHA = "0.5"
        L5_RATIO = "0.5"
    }

    secrets = [
        "DUMMY_SECRET_A",
        "DUMMY_SECRET_B"
    ]
}

serve {
    image = "python:3.7"
    install = ["echo"]
    script = ["cd ../serve && pwd && ls -la && python3 -m http.server $SERVER_PORT"]
}

features {
    image = "basisai/spark-py:v2.4.0r7"
    install = []
    script = ["pwd && ls -la && python hello.py"]

    parameters {
        SUBPROJECT_FEATURE = "true"
        DUMMY_PARAMETER = "0.5"
        EMBEDDING_PATH = "gs://dummy-gcs-bucket/glove.6B.100d.txt"
    }

    secrets = [
        "DUMMY_SECRET_C",
        "DUMMY_SECRET_D"
    ]

    feature_definition {
      name = "pid"
      key = "product_id"
      description = "whether the product is cross-border"
    }

    feature_definition {
      name = "cbp_pid"
      key = "product_id"
      description = "whether the product is cross-border"
    }
}
